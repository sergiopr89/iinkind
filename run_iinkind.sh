#!/bin/bash

# To recreate a cluster, delete it firs with:
# kind delete cluster --name $KIND_CLUSTER

EXECUTION_DIR=$(dirname $(readlink -f $0))
IKD_SANDBOX="${EXECUTION_DIR}/sandbox/"

if [[ -z ${ISTIO_DEFAULT_AUTOINJECT} ]]; then
    ISTIO_DEFAULT_AUTOINJECT="true"
fi

if [[ -z ${KIND_CLUSTER} ]]; then
    KIND_CLUSTER="istio-testing"
fi

echo "Creating ${KIND_CLUSTER} kind cluster"
kind create cluster --name ${KIND_CLUSTER} --config ${EXECUTION_DIR}/kind-config.yaml
if [[ $? -ne 0 ]]; then
    echo "Cannot create ${KIND_CLUSTER} kind cluster" 1>&2
    exit 1
fi

kubectl config use-context kind-${KIND_CLUSTER}
if [[ $? -ne 0 ]]; then
    echo "Cannot switch kubectl context to kind-${KIND_CLUSTER}" 1>&2
    exit 1
fi

echo "Installing Istio..."
istioctl manifest apply --set profile=demo
if [[ $? -ne 0 ]]; then
    echo "Cannot install Istio in ${KIND_CLUSTER} cluster" 1>&2
    exit 1
fi

if [[ ${ISTIO_DEFAULT_AUTOINJECT} == "true" ]]; then
    kubectl label namespace default istio-injection=enabled
fi

echo "DONE!"
