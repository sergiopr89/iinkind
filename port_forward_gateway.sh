#!/bin/bash

EXECUTION_DIR=$(dirname $(readlink -f $0))
IKD_SANDBOX="${EXECUTION_DIR}/sandbox/"

# Set network variabes
export INGRESS_HOST=127.0.0.1
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
export GATEWAY_URL=${INGRESS_HOST}:${INGRESS_PORT}
export SECURE_GATEWAY_URL=${INGRESS_HOST}:${INGRESS_PORT}

kubectl -n istio-system port-forward service/istio-ingressgateway ${INGRESS_PORT}:80 &>/dev/null &
kubectl -n istio-system port-forward service/istio-ingressgateway ${SECURE_INGRESS_PORT}:443 &>/dev/null &
sleep 1 # Time to remove spawned redundant process if already exposed
ps -ef | grep ${INGRESS_PORT}:80 | grep -v grep | tr -s " " | cut -f 2 -d " " > ${IKD_SANDBOX}/gateway-port-forward.pid
ps -ef | grep ${SECURE_INGRESS_PORT}:443 | grep -v grep | tr -s " " | cut -f 2 -d " " > ${IKD_SANDBOX}/secure-gateway-port-forward.pid

echo "# To import variables use the command: source <(./port_forward_gateway.sh)"
echo "export INGRESS_HOST=${INGRESS_HOST}"
echo "export INGRESS_PORT=${INGRESS_PORT}"
echo "export SECURE_INGRESS_PORT=${SECURE_INGRESS_PORT}"
echo "export GATEWAY_URL=${GATEWAY_URL}"
echo "export SECURE_GATEWAY_URL=${SECURE_GATEWAY_URL}"
