#!/bin/bash

EXECUTION_DIR=$(dirname $(readlink -f $0))
IKD_SANDBOX="${EXECUTION_DIR}/sandbox/"

if [[ -z ${KIND_INSTALL_PATH} ]]; then
    KIND_INSTALL_PATH="/usr/local/bin/"
fi

if [[ -z ${KIND_BINARY_NAME} ]]; then
    KIND_BINARY_NAME="kind"
fi

if [[ -f ${KIND_INSTALL_PATH}/${KIND_BINARY_NAME} ]]; then
  echo "${KIND_INSTALL_PATH}/${KIND_BINARY_NAME} already installed"
  exit 0
fi

echo "Downloading KinD..."

curl -Lo ${IKD_SANDBOX}/${KIND_BINARY_NAME} https://github.com/kubernetes-sigs/kind/releases/download/v0.7.0/kind-$(uname)-amd64
if [[ $? -ne 0 ]]; then
    echo "Cannot install kind, take a look at the errors while downloading the file" 1>&2
    exit 1
fi

chmod +x ${IKD_SANDBOX}/${KIND_BINARY_NAME}
sudo mv ${IKD_SANDBOX}/${KIND_BINARY_NAME} ${KIND_INSTALL_PATH}/${KIND_BINARY_NAME}
if [[ $? -ne 0 ]]; then
    echo "Cannot move ${IKD_SANDBOX}/${KIND_BINARY_NAME} to ${KIND_INSTALL_PATH}/${KIND_BINARY_NAME}" 1>&2
    exit 1
fi

sudo chown root. ${KIND_INSTALL_PATH}/${KIND_BINARY_NAME}
if [[ $? -ne 0 ]]; then
    echo "Cannot set root ownership to ${KIND_INSTALL_PATH}/${KIND_BINARY_NAME}" 1>&2
    exit 1
fi

echo "KinD was Succesfully installed in ${KIND_INSTALL_PATH}/${KIND_BINARY_NAME}"
