#!/bin/bash

EXECUTION_DIR=$(dirname $(readlink -f $0))
IKD_SANDBOX="${EXECUTION_DIR}/sandbox/"

if [[ -z ${ISTIO_VERSION} ]]; then
    ISTIO_VERSION="1.5.1"
fi

cd ${IKD_SANDBOX}/istio-${ISTIO_VERSION}

kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml
kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml

if [[ ! -z ${GATEWAY_URL} ]]; then
    echo -e "\nAccess to the example --> http://${GATEWAY_URL}/productpage"
fi
