# IinKinD: Istio in Kubernetes in Docker
A simple lightweight way to deploy and test Istio locally

## Install KinD
If you don't have KinD already:
```
./install_kind.sh
```

## Get Istio and install istioctl
If you don't have Istio already, it will download by default the 1.5.1 version
```
./get_istio.sh
```

## Get IinKinD running
It will create by default a cluster with 1 master and 3 workers, then install Istio
```
./run_iinkind.sh
```

## Port forward the Ingress Gateway
It will map the same node ports into your host for 80 and 443 gateway ports. You can optionally (recommended) get a set of variables set by the script if you source the stdin like:
```
source <(./port_forward_gateway.sh)
```

## Install the example
You can install the example and if you previously executed the port forward and sourced the output variables, it will print the endpoint
```
./install_example.sh
```
