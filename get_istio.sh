#!/bin/bash

EXECUTION_DIR=$(dirname $(readlink -f $0))
IKD_SANDBOX="${EXECUTION_DIR}/sandbox/"

if [[ -z ${ISTIO_INSTALL_PATH} ]]; then
    ISTIO_INSTALL_PATH="/usr/local/bin/"
fi

if [[ -z ${ISTIO_VERSION} ]]; then
    ISTIO_VERSION="1.5.1"
fi

if [[ -z ${ISTIO_BINARY_NAME} ]]; then
    ISTIO_BINARY_NAME="istioctl"
fi

if [[ -f ${ISTIO_INSTALL_PATH}/${ISTIO_BINARY_NAME} ]]; then
  echo "${ISTIO_INSTALL_PATH}/${ISTIO_BINARY_NAME} already installed"
  exit 0
fi

echo "Downloading Istio..."

# curl -Lo ${IKD_SANDBOX}/${ISTIO_BINARY_NAME} https://github.com/kubernetes-sigs/kind/releases/download/v0.7.0/kind-$(uname)-amd64
cd ${IKD_SANDBOX}
rm -rf istio-${ISTIO_VERSION}
curl -L https://istio.io/downloadIstio | sh -
if [[ $? -ne 0 ]]; then
    echo "Cannot install Istio, take a look at the errors while downloading the file" 1>&2
    exit 1
fi

chmod +x istio-${ISTIO_VERSION}/bin/${ISTIO_BINARY_NAME}
sudo cp istio-${ISTIO_VERSION}/bin/${ISTIO_BINARY_NAME} ${ISTIO_INSTALL_PATH}/${ISTIO_BINARY_NAME}
if [[ $? -ne 0 ]]; then
    echo "Cannot move ${IKD_SANDBOX}/${ISTIO_BINARY_NAME} to ${ISTIO_INSTALL_PATH}/${ISTIO_BINARY_NAME}" 1>&2
    exit 1
fi

sudo chown root. ${ISTIO_INSTALL_PATH}/${ISTIO_BINARY_NAME}
if [[ $? -ne 0 ]]; then
    echo "Cannot set root ownership to ${ISTIO_INSTALL_PATH}/${ISTIO_BINARY_NAME}" 1>&2
    exit 1
fi

echo "istioctl was Succesfully installed in ${ISTIO_INSTALL_PATH}/${ISTIO_BINARY_NAME}"
